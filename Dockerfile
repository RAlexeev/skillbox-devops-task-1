# Base image which contains global dependencies
FROM node

# Update and install system dependencies
RUN apt-get update && \
    apt-get install -y curl git

# Go to App directory
RUN mkdir /app
WORKDIR /app

# Download and install App dependencies
RUN curl https://raw.githubusercontent.com/timurb/flatris/master/package.json -o package.json -s
RUN curl https://raw.githubusercontent.com/timurb/flatris/master/yarn.lock -o yarn.lock -s
RUN yarn install --pure-lockfile

# Clone the App files into the docker container
RUN git clone https://github.com/timurb/flatris flatris/
RUN mv node_modules flatris/

# Test and build the App
WORKDIR /app/flatris
RUN yarn test
RUN yarn build

# Run the App
CMD yarn start

# Open the network port to have an external access to the App
EXPOSE 3000
