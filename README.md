# Skillbox task running Flatris app in a Docker container

## How to run

You can run the app in a docker container with or without Docker Compose. Choose a way which you prefer.

### Build and run the App manually

```sh
docker build . -t skillbox
docker run -it -p 3000:3000 skillbox
```

### Run the App using Docker Compose

```sh
docker-compose up
```
